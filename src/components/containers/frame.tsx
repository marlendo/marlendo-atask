import React from "react";
import { isBrowser } from "react-device-detect";
import { DeviceFrameset } from "react-device-frameset";
import "react-device-frameset/styles/marvel-devices.min.css";

const MainFrame = (props: { children: any; }) => {
  if (isBrowser) {
    return (
      <div
        className={
          "w-[100vw] h-[100vh] flex justify-center items-center overflow-hidden bg-gray-700"
        }
      >
        <DeviceFrameset device={"Galaxy Note 8"} color="gold">
          <>{props.children}</>
        </DeviceFrameset>
      </div>
    );
  }

  return props.children;
};

export default MainFrame;
