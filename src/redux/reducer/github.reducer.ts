import { GITHUB_API_FAILED, GITHUB_API_REQUEST, GITHUB_API_SUCCESS } from "../../constant/redux.constant";

const initialState = {
  data: [],
  isLoading: false,
  error: null,
};

export const githubReducer = (
  state = initialState,
  action: { type: any; payload: any }
) => {
  switch (action.type) {
    case GITHUB_API_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case GITHUB_API_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    case GITHUB_API_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};
