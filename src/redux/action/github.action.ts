import { GITHUB_API_FAILED, GITHUB_API_REQUEST, GITHUB_API_SUCCESS } from "../../constant/redux.constant";

export const fetchGithubApi = () => {
  return async (dispatch: (arg0: { type: string; payload?: any; }) => void) => {
    dispatch({ type: GITHUB_API_REQUEST });
    try {
      const data = await fetch("https://api.example.com/data");
      const json = await data.json();
      dispatch({ type: GITHUB_API_SUCCESS, payload: json });
    } catch (error) {
      dispatch({ type: GITHUB_API_FAILED, payload: error });
    }
  };
};
