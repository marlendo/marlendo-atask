import React from "react";
import ReactDOM from "react-dom/client";
import MainFrame from "./components/containers/frame";
import "./index.css";
import App from "./pages/github";
import reportWebVitals from "./reportWebVitals";

import { compose } from "redux";
import { Provider } from "react-redux";
import store from "./redux/store";

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}


const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <MainFrame>
        <App />
      </MainFrame>
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
