import React from "react";

const GithubPage = () => {
  return (
    <div>
      <div className="relative">
          <input
            type="text"
            id="simple-email"
            className="flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-gray-100 text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-700 focus:border-transparent"
            placeholder="Your email"
          />
        </div>
    </div>
  );
}

export default GithubPage
